module.exports = {
  title: 'VuePress',
  description: 'VuePress on GitLab Pages',
  dest: 'public',
  base: '/pages_vue/',
  themeConfig: {
    // Full GitLab url to repo.
    repo: 'https://gitlab.com/ckatanda/pages_vue',
    // defaults to true, set to false to disable
    editLinks: false,
    // custom text for edit link. Defaults to "Edit this page"
    editLinkText: 'Help us improve this page!'
  }
}
